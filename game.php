<?php
game();

function game()
{
    fwrite(STDOUT, "########################### \n Anything besides numbers will be ignored \n########################### \n");

    fwrite(STDOUT, "Please enter number of trees: \n");
    $trees = fgets(STDIN);
    fwrite(STDOUT, "Please enter number of houses: \n");
    $houses = fgets(STDIN);
    fwrite(STDOUT, "Please enter number of cars: \n");
    $cars = fgets(STDIN);
    fwrite(STDOUT, "Game World: \n");
    $args = ['tree' => $trees, 'house' => $houses, 'car' => $cars];

    require_once('Class/GameWorld.php');
    $gameWorld = new GameWorld($args);
    $gameWorld->render();
}

?>


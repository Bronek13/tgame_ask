<?php


require_once('Interface/IRendelable.php');

class GameElement implements IRendelable
{
    protected $name;

    public function render()
    {
        echo 'Renderuję ' . $this->name . "\r\n";
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return GameElement
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
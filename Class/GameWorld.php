<?php

require_once('Interface/IRendelable.php');
require_once('Class/GameElements/Car.php');
require_once('Class/GameElements/House.php');
require_once('Class/GameElements/Tree.php');

class GameWorld implements IRendelable
{
    public $elements = [];

    public function __construct(array $elements)
    {
        $newElements = [];

        foreach ($elements as $key => $value) {
            for ($i = 0; $i < $value; $i++) {
                if ($key === 'car') {
                    $newElements[] = new Car();
                }
                if ($key === 'house') {
                    $newElements[] = new House();
                }
                if ($key === 'tree') {
                    $newElements[] = new Tree();
                }
            }

        }

        shuffle($newElements);
        $this->elements = $newElements;
    }

    public function render()
    {
        foreach ($this->elements as $element) {
            $element->render();
        }

    }
}